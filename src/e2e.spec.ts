import { Clock, timeControl } from './app/control/time-control';
import { CommandBus, initCommandHandlers, Command } from './app/control/command-control';
import { Pet } from './app/domain';

/**
 * due to time limit, only cover feed command end to end test
 * if time allows, more end to end test needs to be donw
 */
describe("Test suite for end to end", () => {

    describe("end to end test for feed command", () => {
        let pet: Pet;
        let commandBus: CommandBus;
        beforeEach(() => {
            const clock = new Clock();
            pet = Pet.birth('Charlie', clock.year());
            commandBus = new CommandBus();
            initCommandHandlers(pet).forEach((handler, cmd) => {
                commandBus.register(cmd, handler);
            });
        });
        test("corner case, when feed to new born pet, hunger index is expected to be 0 and lonely index is expected to be -1", () => {
            expect(pet.hunger).toBe(0);
            expect(pet.lonely).toBe(0);
            commandBus.send('feed');
            expect(pet.hunger).toBe(0);
            expect(pet.lonely).toBe(-1);
        });
        test("when feed non new born alive pet, hunger index is expected to be 0 and lonely index is expected to be 1 less", () => {
            pet.unattended();
            pet.unattended();
            expect(pet.hunger).toBe(2);
            expect(pet.lonely).toBe(2);
            commandBus.send('feed');
            expect(pet.hunger).toBe(0);
            expect(pet.lonely).toBe(1);
        });
        test("corner case, when feed dead pet, hunger and lonely index is expected to be unchanged", () => {
            pet.unattended();
            pet.unattended();
            expect(pet.hunger).toBe(2);
            expect(pet.lonely).toBe(2);
            pet.die();
            commandBus.send('feed');
            expect(pet.hunger).toBe(2);
            expect(pet.lonely).toBe(2);
        });
    });

    // Unit tests for other services
});