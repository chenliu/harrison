import { Subject, } from 'rxjs';
import inquirer from 'inquirer';

import { CommandBus } from '../control/command-control';

function makePrompt(promptMessage: string, i = 0) {
    return {
        type: 'input',
        name: `n${i}`,
        message: `${promptMessage}>`
    };
}

export function cli(promptMessage: string, commandBus: CommandBus, timeControl: number): void {
    const prompts = new Subject<any>();
    let i = 0;
    const ui = inquirer.prompt(prompts).ui;
    ui.process.subscribe(
        (answers) => {
            if (answers.answer === 'q') {
                prompts.complete();
                clearInterval(timeControl);
            } else {
                if (answers.answer !== '') {
                    commandBus.send(answers.answer);
                }
                i += 1;
                prompts.next(makePrompt(promptMessage, i));
            }
        },
        (error: Error) => {
            console.error(error);
        },
        () => {
            console.log("bye!!!!!!");
        }
    );

    prompts.next(makePrompt(promptMessage));
}
