import { Clock } from '../control/time-control';
import { Pet } from '../domain';

const SLEEP_TIME = '20:00:00';
const MAX_AGE = 3;
const MAX_HUNGER_INDEX = 20;
const MAX_UNHEALTHY_INDEX = 30;
const MAX_LONELY_INDEX = 40;

/**
 * All pet services are provided here
 */


function autoSleep(pet: Pet, clock: Clock): void {
    if (clock.currentTime() === SLEEP_TIME) {
        pet.sleep();
    }
}

export function checkPetAge(pet: Pet, clock: Clock): void {
    if (pet.age(clock.year()) >= MAX_AGE) {
        pet.die();
        console.log('too old, died');
    }
}

export function checkPetHungerStatus(pet: Pet): void {
    if (pet.hunger >= MAX_HUNGER_INDEX) {
        pet.die();
        console.log('too hungry, died');
    }
}

export function checkPetHealthStatus(pet: Pet): void {
    if (pet.unhealthy >= MAX_UNHEALTHY_INDEX) {
        pet.die();
        console.log('unhealthy, died');
    }
}

export function checkPetLonelinessStatus(pet: Pet): void {
    if (pet.lonely >= MAX_LONELY_INDEX) {
        pet.die();
        console.log('too lonely, died');
    }
}


function printPetStatus(pet: Pet, clock: Clock): void {
    const status = `date=${clock.currentDate()}, age=${pet.age(clock.year())}, hungerIndex=${pet.hunger}, unhealthyIndex=${pet.unhealthy}, lonelyIndex=${pet.lonely}`;
    console.log(status);
}

export function onTimeChange(pet: Pet, clock: Clock): void {
    if (!pet.isAlive) {
        return;
    }
    pet.unattended();
    checkPetAge(pet, clock);
    checkPetHungerStatus(pet);
    checkPetHealthStatus(pet);
    checkPetLonelinessStatus(pet);
    autoSleep(pet, clock);
    printPetStatus(pet, clock);
}