import { Pet } from "../domain";
import { Clock } from "../control/time-control";
import { checkPetAge } from ".";

/**
 * due to time limit, only cover checkPetAge test
 * if time allows, all services will be tested with happy scenairos and error scenarios
 */
describe("Test suite for pet service", () => {

    describe("Unit tests for checkPetAge service", () => {
        let pet: Pet;
        let clock: Clock;
        beforeEach(() => {
            const clock = new Clock();
            pet = Pet.birth('Charlie', clock.year());
        });
        test("corner case, new born pet age is expected to be 0 and is alive", () => {
            clock = new Clock();
            checkPetAge(pet, clock);
            expect(pet.age(clock.year())).toBe(BigInt(0));
            expect(pet.isAlive).toBe(true);
        });
        test("pet less than 3 years old, it is alive", () => {
            clock = new Clock(BigInt(260000));
            checkPetAge(pet, clock);
            expect(pet.age(clock.year())).toBe(BigInt(1));
            expect(pet.isAlive).toBe(true);
        });
        test("corner case, pet exact 3 years old, it will die", () => {
            clock = new Clock(BigInt(1020000));
            checkPetAge(pet, clock);
            expect(pet.age(clock.year())).toBe(BigInt(3));
            expect(pet.isAlive).toBe(false);
        });
    });

    // Unit tests for other services
});