import { Command, CommandHandler } from ".";

/**
 * CommandBus used command pattern to handle the command
 */
export class CommandBus {
    private handlers: Map<string, CommandHandler>;

    constructor() {
        this.handlers = new Map();
    }

    register(command: string, handler: CommandHandler): void {
        this.handlers.set(command, handler);
    }

    showCommands(): string {
        return `${Array.from(this.handlers.keys())}`;
    }

    send(command: Command): void {
        const handler = this.handlers.get(command);
        if (handler) {
            handler();
        } else {
            console.error(`Wrong command: '${command}', please type: ${this.showCommands()}`);
        }
    }
}