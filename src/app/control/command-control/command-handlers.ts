import { Pet } from '../../domain';
import { Command, CommandHandler } from '.';

export function initCommandHandlers(pet: Pet): Map<Command, CommandHandler> {
    const handlers = new Map<Command, CommandHandler>();
    handlers.set('feed', () => pet.feed());
    handlers.set('sleep', () => pet.sleep());
    handlers.set('visit', () => pet.visit());
    return handlers;
}