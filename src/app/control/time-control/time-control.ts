import { Clock } from ".";

import { Pet } from "../../domain";
import { onTimeChange } from "../../service";

/**
 * This function provides a time scheduling service to advance the virtual clock and trigger time change actions
 * @param pet 
 * @param clock 
 */
export function timeControl(pet: Pet, clock: Clock): number {
    return setInterval(() => {
        // tick virtual clock
        clock.tick();
        // sampling rate is set to hourly by virtual clock
        if (clock.isHour()) {
            // every virtual clock hour, pet status checking will be triggerd, pet state may be transferred to new state.
            onTimeChange(pet, clock);
        }
    });
}
