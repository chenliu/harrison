const SECONDS_PER_MINUTE = BigInt(60);
const MINUTES_PER_HOUR = BigInt(60);
const HOURS_PER_DAY = BigInt(24);
// game clock: assume 3 days a year
const DAYS_PER_YEAR = BigInt(3);

// game clock: assume a tick is a second, a tick is about a mili second of a real clock
const TICKS_PER_SECOND = BigInt(1);
const TICKS_PER_MINUTE = TICKS_PER_SECOND * SECONDS_PER_MINUTE;
const TICKS_PER_HOUR = TICKS_PER_MINUTE * MINUTES_PER_HOUR;
const TICKS_PER_DAY = TICKS_PER_HOUR * HOURS_PER_DAY;
const TICKS_PER_YEAR = TICKS_PER_DAY * DAYS_PER_YEAR;

/**
 * A virtual clock to simulate the real clock so that the pet can complete the life cycle faster
 */
export class Clock {
    constructor(private ticks = BigInt(0)) { }
    tick(): void {
        this.ticks += BigInt(1);
    }

    seconds(): bigint {
        return this.ticks % SECONDS_PER_MINUTE;
    }

    minutes(): bigint {
        return this.ticks / TICKS_PER_MINUTE % MINUTES_PER_HOUR;
    }

    hours(): bigint {
        return this.ticks / TICKS_PER_HOUR % HOURS_PER_DAY;
    }

    year(): bigint {
        return this.ticks / TICKS_PER_YEAR;
    }

    isHour(): boolean {
        return this.minutes() === BigInt(0) && this.seconds() === BigInt(0);
    }

    currentTime(): string {
        const hh = `${this.hours()}`.padStart(2, '0');
        const mm = `${this.minutes()}`.padStart(2, '0');
        const ss = `${this.seconds()}`.padStart(2, '0');
        return `${hh}:${mm}:${ss}`;
    }

    currentDate(): string {
        const yy = `${this.year()}`.padStart(2, '0');
        const currentTime: string = this.currentTime();
        return `${yy}-${currentTime}`;
    }
}