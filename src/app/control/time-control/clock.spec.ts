import { Clock } from "./clock";

describe("game clock test", () => {
    test("test year", () => {
        const clock = new Clock(BigInt(259999));
        clock.tick();
        expect(BigInt(1)).toBe(clock.year());
    });
});