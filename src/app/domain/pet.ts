function deadMessage() {
    console.log('pet is dead!');
}

/**
 * Pet domain entiry with behaviors
 */
export class Pet {
    private hungerIndex = 0;
    private lonelyIndex = 0;
    private unhealthyIndex = 0;
    private alive = true;

    constructor(readonly name: string, readonly yearOfBirth: bigint) { }

    age(currentYear: bigint): bigint {
        return currentYear - this.yearOfBirth;
    }

    feed(): void {
        if (!this.alive) {
            deadMessage();
            return;
        }
        console.log('eating...\n');
        this.hungerIndex = 0;
        this.lonelyIndex -= 1;
    }

    sleep(): void {
        if (!this.alive) {
            deadMessage();
            return;
        }
        console.log('going to sleep...\n');
        this.unhealthyIndex -= 1;
        this.lonelyIndex -= 1;
    }

    visit(): void {
        if (!this.alive) {
            deadMessage();
            return;
        }
        console.log('being visited...\n');
        this.lonelyIndex -= 1;
    }

    unattended(): void {
        this.hungerIndex += 1;
        this.lonelyIndex += 1;
        this.unhealthyIndex += 1;
    }

    die(): void {
        this.alive = false;
    }

    get hunger(): number {
        return this.hungerIndex;
    }

    get lonely(): number {
        return this.lonelyIndex;
    }

    get unhealthy(): number {
        return this.unhealthyIndex;
    }
    get isAlive(): boolean {
        return this.alive;
    }

    static birth(name: string, yearOfBirth: bigint): Pet {
        return new Pet(name, yearOfBirth);
    }
}
