import { Pet } from ".";
import { Clock } from "../control/time-control";

/**
 * due to time limit, only cover pet.feed test
 * normally, the unit tests hould cover all the pet behaviors, all corner cases, happy path and error path.
 */
describe("Test suite for pet entity", () => {

    describe("Unit tests for pet.feed behavior", () => {
        let pet: Pet;
        beforeEach(() => {
            const clock = new Clock();
            pet = Pet.birth('Charlie', clock.year());
        });
        test("corner case, when feed to new born pet, hunger index is expected to be 0 and lonely index is expected to be -1", () => {
            expect(pet.hunger).toBe(0);
            expect(pet.lonely).toBe(0);
            pet.feed();
            expect(pet.hunger).toBe(0);
            expect(pet.lonely).toBe(-1);
        });
        test("when feed non new born alive pet, hunger index is expected to be 0 and lonely index is expected to be 1 less", () => {
            pet.unattended();
            pet.unattended();
            expect(pet.hunger).toBe(2);
            expect(pet.lonely).toBe(2);
            pet.feed();
            expect(pet.hunger).toBe(0);
            expect(pet.lonely).toBe(1);
        });
        test("corner case, when feed dead pet, hunger and lonely index is expected to be unchanged", () => {
            pet.unattended();
            pet.unattended();
            expect(pet.hunger).toBe(2);
            expect(pet.lonely).toBe(2);
            pet.die();
            pet.feed();
            expect(pet.hunger).toBe(2);
            expect(pet.lonely).toBe(2);
        });
    });

    // other unit tests
});