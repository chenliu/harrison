import clear from 'clear';
import figlet from 'figlet';
import chalk from 'chalk';

import { Clock, timeControl } from './app/control/time-control';
import { cli } from './app/cli';
import { CommandBus, initCommandHandlers, Command } from './app/control/command-control';
import { Pet } from './app/domain';

function help(commands: Command[]) {
    console.log('===== commands =====');
    console.log('quit app: q');
    console.log(`pet care: ${commands}`);
    console.log('====================');
}

function initCliStyle() {
    clear();
    console.log(
        chalk.yellow(
            figlet.textSync('Tamagotchi', { horizontalLayout: 'full' })
        )
    );
    help(['feed', 'visit', 'sleep']);
}

function initCli() {
    /**
     * TODO: use IOC framework to control the dependencies
     * due to time limit, the object are created and wired here to follow the dependency inversion principle
     */
    const clock = new Clock();
    const pet = Pet.birth('Charlie', clock.year());
    const commandBus = new CommandBus();
    initCommandHandlers(pet).forEach((handler, cmd) => {
        commandBus.register(cmd, handler);
    });
    const promptMessage = pet.name;
    cli(promptMessage, commandBus, timeControl(pet, clock));
}

export function main(): void {
    initCliStyle();
    initCli();
}
