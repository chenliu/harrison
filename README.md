# how to run the program

## clean compile output

```
npm run clean
```

## build

```
npm run build
```

## lint

```
npm run lint
```

## test

```
npm test
```

## run

after build, run

```
bin/cli.js
```

or

```
npm link
```

then run

```
tamagotchi
```

## clean up & remove link

```
npm unlink
```

# how to play Tamagotchi game

## Tamagotchi will die after 3 years old

## Tamagotchi will die if too hungery (hunger index >=20)

## Tamagotchi will die if unhealthy (unhealth index >=30)

## Tamagotchi will die if too lonely (lonely index >=40)

## you can feed Tamagotchi (by feed command, hunger index will be set to 0, lonely index will be minus 1)

## you can put Tamagotchi to sleep (by sleep command, unhealth index will be minus 1, lonely index will be minus 1)

## you can visit Tamagotchi (by visit command, lonely index will be minus 1)

# main design about the Tamagotchi game

## it is a simple cli version of Tamagotchi

## inquirer library is used to implement the cli prompt

## there are two main control flow, one is the command control, which use command bus to handle specific user input command to trigger state change; another is the time control, which is a time scheduler to trigger time change related stage change

## pet service provides all the services to check pet status, which is used in time control flow to check and change pet status

## pet domain entity is a rich behavior domain model, which is used in command control flow to change pet status

# test cases

## e2e tests covered the feed command end to end workflow as an example

## pet-service test covered checkPetAge service corner cases and normal scenario as an example

## pet test covered pet.feed corner cases and normal behavior as an example

# things need to be improved

## due to the time constraint, the test coverage is not enough. In reality, all services, domain entity behavior and important data flow need to be convered. All interesting corner cases, error scenarios and happy scenarios need to be covered.

## The model and assupmtion is very simple for test purpose only. In reality, more realistic and attactive model and game experience need to designed.

## containerize the artifact

## build ci/cd pipeline to automate build/test/publish/deploy lifecycle
